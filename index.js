var express = require("express");
var socket = require("socket.io");

// App setup
var app = express();
var server = app.listen(4000, () => {
  console.log("Listening to port 4000");
});

// Serve static files
app.use(express.static("public"));

// Socket
var io = socket(server);

io.on("connection", (socket) => {
  console.log("Socket connected!", socket.id);

  socket.on("chat", (data) => {
    io.sockets.emit("chat", { ...data, socketId: socket.id });
  });

  socket.on("typing", (data) => {
    socket.broadcast.emit("typing", { ...data, socketId: socket.id });
  });

  socket.on("stop-typing", (data) => {
    socket.broadcast.emit("stop-typing", { ...data, socketId: socket.id });
  });
});

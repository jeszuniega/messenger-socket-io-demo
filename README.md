# Messenger Socket io demo

Socket io messenger demo

Reference: [The Net Ninja youtube tutorial playlist](https://www.youtube.com/watch?v=FvArk8-qgCk&list=PL4cUxeGkcC9i4V-_ZVwLmOusj8YAUhj_9&index=6)

# Setup

Run `npm install`

# To run locally

Run `npm start`

var socket = io.connect(location.origin);

// Query DOM
var message = document.getElementById("message"),
  handle = document.getElementById("handle"),
  btn = document.getElementById("send"),
  output = document.getElementById("output");
feedback = document.getElementById("feedback");

let typingTimeout = null;
const typingExpire = 1000;

const chat = () => {
  if (message.value.trim() === "") return true;
  socket.emit("chat", {
    message: message.value.trim(),
    handle: handle.value.trim() || "Anonymous",
  });
  stopTyping();
  message.value = "";
};

const typing = () => {
  socket.emit("typing", {
    handle: handle.value,
  });
};

const stopTyping = () => {
  clearTimeout(typingTimeout);
  socket.emit("stop-typing", {
    handle: handle.value,
  });
};

// On click send button event
btn.addEventListener("click", (e) => {
  chat();
});

message.addEventListener("input", (e) => {
  clearTimeout(typingTimeout);
  typing();
  typingTimeout = setTimeout(() => {
    stopTyping();
  }, typingExpire);
});

// Listen events
socket.on("chat", (data) => {
  const me = data.socketId === socket.id;
  output.innerHTML += `<p ${me && 'class="me"'}><strong>${
    me ? "You" : data.handle
  }</strong>: ${data.message}</p>`;

  // scroll to the latest chat
  output.lastChild.scrollIntoView(output.scrollHeight);
});

socket.on("typing", (data) => {
  data.handle = data.handle?.trim();
  const name = data.handle !== "" ? data.handle : "Someone";
  document.getElementById(`socket-${data.socketId}`)?.remove();
  feedback.innerHTML += `<p id="socket-${data.socketId}"><em>${name} is typing...</em></p>`;
});

socket.on("stop-typing", (data) => {
  document.getElementById(`socket-${data.socketId}`)?.remove();
});
